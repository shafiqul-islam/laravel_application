<?php

namespace App\Http\Controllers;

use App\Models\Like;
use Illuminate\Http\Request;

class LikeController extends Controller
{
    public function index()
    {
        $data = Like::get();
        return view('theme.likes.all', ['data' => $data]);
    }
    
    public function store(Request $request)
    {
        $likeData = new Like;

        $likeData->like = $request->like;
        $likeData->save();
    }
}
