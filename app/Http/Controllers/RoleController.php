<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function index()
    {
        return view('theme.role.all');
    }

    public function add(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required',
        ]);

        $addRole = new Role;
        $addRole->name = $request->name;
        $response = $addRole->save();

        if ($response) {
            return redirect('/all-role');
        } else {
            return redirect('/all-role');
        }
    }

    public function destroy($id)
    {
        $deleteRole = Role::findOrFail($id)->delete();

        if ($deleteRole) {
            return redirect()->back();
        } else {
            return redirect()->back();
        }
    }

    public function fetchRoleData(Request $request)
    {
        $validator = $request->validate([
            'roleID' => 'required',
        ]);

        $RoleData = Role::findOrFail($request->roleID);

        if ($validator) {
            echo json_encode($RoleData);
        } else {
            echo json_encode(false);
        }
    }

    public function update(Request $request)
    {
        $updateRole = Role::findOrFail($request->id);

        $validator = $request->validate([
            'name' => 'required',
        ]);

        $updateRole->name = $request->name;
        $response = $updateRole->save();

        if ($response) {
            return redirect('/all-role');
        } else {
            return redirect('/all-role');
        }
    }

    public function fetchByAjaxAllRoles(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'name',
        );

        $TotalRecords = Role::count();

        $roles = Role::orderBy($columns[$request['order'][0]['column']], $request['order'][0]['dir'])
            ->offset($request['start'])
            ->limit($request['length'])
            ->get();

        $TotalFiltered = $TotalRecords;

        if ($roles) {
            $rows = [];
            foreach ($roles as $role) {

                $actions = '<div class="d-flex align-item-center">
                    <a href="" data-role-id="' . $role->id . '" class="btn btn-sm p-2 edit_role_btn" data-bs-toggle="modal" data-bs-target=".edit_new_role_modal">
                    <i class="fas fa-pen-nib text-success"></i>
                    </a>

                    <a href="#" class="btn btn-sm p-2">
                    <form action="' .  url('/delete-role', [$role->id]) . '" method="POST">
                    ' . method_field("DELETE") . '
                    ' . csrf_field() . '
                        <i class="fas fa-times text-danger remove"></i>
                    </form>
                    </a> </div>';

                $permission = '<a href="' . url('/all-permissions', [$role->id]) . '">
                     All Permission
                     </a>';

                $td = [];
                $td[] = $role->id;
                $td[] = $role->name;
                $td[] = $permission;
                $td[] = $actions;
                $rows[] = $td;
            }
            $json_data = array(
                "draw" => intval($request->draw),
                "recordsTotal" => intval($TotalRecords),
                "recordsFiltered" => intval($TotalFiltered),
                "data" => $rows,
            );
            echo json_encode($json_data);
        }
    }

    public function permission($id)
    {
        $permissionArr = [];
        $role = Role::findOrFail($id);

        $permissions = $role->permissions;
        foreach ($permissions as $permission) {
            $permissionArr[$permission->name] = $permission->name;
        }
        return view('theme.role.all_permission', ['role' => $role, 'permissions' => $permissionArr]);
    }

    public function updatePermission(Request $request)
    {
        $role = $request->role;
        $permissionName = $request->permission;

        $permissionRes = Permission::where('name', '=' , $permissionName)->count();
        if($permissionRes <= 0){
            $permission = Permission::create(['name' => $permissionName]);
        }else{
            $permission = Permission::getPermissions(['name' => $permissionName])->first();
        }

        $role = Role::find($role);
        $roleHasPermission = $role->hasPermissionTo($permissionName);
        if($roleHasPermission){
            $role->revokePermissionTo($permissionName);            
        }else{
            $permission->assignRole($role);
        }

        echo json_encode(1);

    }
}
