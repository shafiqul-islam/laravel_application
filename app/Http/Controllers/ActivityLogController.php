<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\ActivityLog;
use Illuminate\Http\Request;
// use Illuminate\Routing\Controller;

class ActivityLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('theme.Activity_log.all');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function fetchByAjaxLogs(Request $request)
    {

        $columns = array(
            0 => 'id',
            1 => 'log_name',
            2 => 'description',
            3 => 'subject_type',
            4 => 'event',
            5 => 'subject_id',
            6 => 'causer_id',
            7 => 'created_at',
            8 => 'updated_at',
        );

        $TotalRecords = ActivityLog::count();

        $logs = ActivityLog::orderBy($columns[$request['order'][0]['column']], $request['order'][0]['dir'])
            ->offset($request['start'])
            ->limit($request['length'])
            ->get();


        $TotalFiltered = $TotalRecords;

        if ($logs) {
            $rows = [];
            foreach ($logs as $log) {



                $td = [];
                $td[] = $log->id;
                $td[] = $log->log_name;
                $td[] = $log->description;
                $td[] = $log->subject_type;
                $td[] = $log->event;

                if (isset($log->subjectID) && !empty($log->subjectID)) {
                    $td[] = $log->subjectID->username;
                } else {
                    $td[] = $log->subject_id;
                }

                if (isset($log->causerID) && !empty($log->causerID)) {
                    $td[] = $log->causerID->name;
                } else {
                    $td[] = $log->causer_id;
                }
                
                $td[] = $log->created_at;
                $td[] = $log->updated_at;
                // $td[] = $actions;
                $rows[] = $td;
            }
            $json_data = array(
                "draw" => intval($request->draw),
                "recordsTotal" => intval($TotalRecords),
                "recordsFiltered" => intval($TotalFiltered),
                "data" => $rows,
            );
            echo json_encode($json_data);
        }
    }
}
