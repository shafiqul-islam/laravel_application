<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{
    public function index()
    {
        $roles = Role::all();
        return view('theme.users.all', ['roles' => $roles]);
    }

    public function add(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required',
            // 'role' => 'required',
            'password' => 'required',
            'email' => 'required|unique:users',
            'address' => 'required',
            'phone' => 'required|unique:users',
        ]);

        $addUser = new User;
        $addUser->name = $request->name;
        $addUser->role = $request->role;
        $addUser->email = $request->email;
        $addUser->country = $request->country;
        $addUser->city = $request->city;
        $addUser->phone = $request->phone;
        $addUser->address = $request->address;
        $addUser->password = $request->password;

        $response = $addUser->save();

        if ($response) {
            return redirect()->back();
        } else {
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        $editProfile = User::findOrFail($id);

        if (isset($editProfile->role) && !empty($editProfile->role)) {
            $roleName = Role::find($editProfile->role)->name;
        }

        $roles = Role::all();

        return view('theme.users.edit', ['editProfile' => $editProfile, 'roles' => $roles, 'roleName' => $roleName]);
    }



    public function destroy($id)
    {
        $deleteProfile = User::findOrFail($id)->delete();

        if ($deleteProfile) {
            return redirect()->back();
        } else {
            return redirect()->back();
        }
    }

    public function update(Request $request)
    {
        $updateUser = User::findOrFail($request->id);

        $validator = $request->validate([
            'name' => ['required', Rule::unique('users')->ignore($updateUser->id)],
            'email' => ['required', Rule::unique('users')->ignore($updateUser->id)],
            'phone' => ['required', Rule::unique('users')->ignore($updateUser->id)],
        ]);

        $updateUser->name = $request->name;
        $updateUser->role = $request->role;
        $updateUser->email = $request->email;
        $updateUser->country = $request->country;
        $updateUser->city = $request->city;
        $updateUser->phone = $request->phone;
        $updateUser->address = $request->address;

        $response = $updateUser->save();

        if ($response) {
            return redirect('/all-users');
        } else {
            return redirect('/all-users');
        }
    }

    public function changeUserPassword(Request $request)
    {
        $validator = $request->validate([
            'password' => 'required|same:confirm_password',
            'confirm_password' => 'required',
        ]);

        $changePassword = User::findOrFail($request->id);

        $changePassword->password =  Hash::make($request->password);

        $response = $changePassword->save();

        if ($response) {
            return redirect('/all-users');
        } else {
            return redirect('/all-users');
        }
    }

    // public function changeUserPhoto(Request $request)
    // {
    //     $validator = $request->validate([
    //         'photo' => 'required|mimes:jpeg,png,jpg,gif,svg',
    //     ]);

    //     if ($request->hasFile('photo')) {

    //         $photo       = $request->file('photo');
    //         $filename    = $photo->getClientOriginalName();

    //         $image_resize = Image::make($photo->getRealPath());
    //         $image_resize->resize(300, 300);
    //         $image_resize->save(public_path('theme/assets/media/user/photos/' . $filename));
    //     }
    // }

    public function changePhoto(Request $request)
    {
        $validator = $request->validate([
            'photo' => 'required|mimes:jpeg,png,jpg,gif,svg',
        ]);

        if ($image = $request->file('photo')) {
            $destinationPath = 'theme/assets/media/user/photos/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $input['photo'] = "$profileImage";
        }

        return redirect('/all-users');
    }




    public function fetchByAjaxAllUsers(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'name',
            2 => 'role',
            3 => 'email',
            4 => 'phone',
            5 => 'country',
            6 => 'city',
            7 => 'address',
        );

        $TotalRecords = User::count();

        $users = User::with('roleName')
            ->orderBy($columns[$request['order'][0]['column']], $request['order'][0]['dir'])
            ->offset($request['start'])
            ->limit($request['length'])
            ->get();

        $TotalFiltered = $TotalRecords;

        if ($users) {
            $rows = [];
            foreach ($users as $user) {

                $actions = '<div class="d-flex align-item-center">
                <a href="' . url('/edit-user', [$user->id]) . '" class="btn btn-sm p-2">
                   <i class="fas fa-pen-nib text-success"></i>
                </a>

                    <a href="#" class="btn btn-sm p-2">
                    <form action="' .  url('/delete-user', [$user->id]) . '" method="POST">
                    ' . method_field("DELETE") . '
                    ' . csrf_field() . '
                        <i class="fas fa-times text-danger remove"></i>
                    </form>
                    </a> </div>';


                $td = [];
                $td[] = $user->id;
                $td[] = $user->name;
                if (isset($user->roleName) && !empty($user->roleName)) {
                    $td[] = $user->roleName->name;
                } else {
                    $td[] = $user->role;
                };
                $td[] = $user->email;
                $td[] = $user->phone;
                $td[] = $user->country;
                $td[] = $user->city;
                $td[] = $user->address;
                $td[] = $actions;
                $rows[] = $td;
            }
            $json_data = array(
                "draw" => intval($request->draw),
                "recordsTotal" => intval($TotalRecords),
                "recordsFiltered" => intval($TotalFiltered),
                "data" => $rows,
            );
            echo json_encode($json_data);
        }
    }
}
