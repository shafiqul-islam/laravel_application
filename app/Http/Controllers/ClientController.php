<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use PhpParser\Node\Stmt\Return_;
use Illuminate\Support\Facades\Hash;

class ClientController extends Controller
{

    public function index()
    {
        return view('theme.Clients.all');
    }


    public function add(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required',
            'username' => 'required|unique:clients',
            'email' => 'required|unique:clients',
            'address' => 'required',
            'phone' => 'required|unique:clients',
        ]);

        $addClient = new Client;
        $addClient->name = $request->name;
        $addClient->username = $request->username;
        $addClient->email = $request->email;
        $addClient->password =  Hash::make($request->password);
        $addClient->address = $request->address;
        $addClient->phone = $request->phone;

        $response = $addClient->save();

        if ($response) {
            return redirect('/clients');
        } else {
            return redirect('/clients');
        }
    }


    public function edit($id)
    {
        $editProfile = Client::findOrFail($id);
        // $data['profileData'] = Client::all();
        return view('theme.Clients.edit', ['editProfile' => $editProfile]);
    }


    public function update(Request $request)
    {
        $updateClient = Client::findOrFail($request->id);

        $validator = $request->validate([
            'username' => ['required', Rule::unique('clients')->ignore($updateClient->id)],
            'email' => ['required', Rule::unique('clients')->ignore($updateClient->id)],
            'phone' => ['required', Rule::unique('clients')->ignore($updateClient->id)],
        ]);

        $updateClient->name = $request->name;
        $updateClient->username = $request->username;
        $updateClient->email = $request->email;
        $updateClient->address = $request->address;
        $updateClient->phone = $request->phone;

        $response = $updateClient->save();

        if ($response) {
            return redirect('/clients');
        } else {
            return redirect('/clients');
        }
    }


    public function destroy($id)
    {
        $deleteProfile = Client::findOrFail($id)->delete();

        if ($deleteProfile) {
            return redirect('/clients');
        } else {
            return redirect('/clients');
        }
    }

    public function changePassword(Request $request)
    {
        $validator = $request->validate([
            'password' => 'required|same:confirm_password',
            'confirm_password' => 'required',
        ]);

        $changePassword = Client::findOrFail($request->id);

        $changePassword->password =  Hash::make($request->password);

        $response = $changePassword->save();

        if ($response) {
            return redirect('/clients');
        } else {
            return redirect('/clients');
        }
    }

    public function fetchByAjaxAllClients(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'name',
            2 => 'username',
            3 => 'email',
            4 => 'address',
            5 => 'phone',
        );

        // $data = Client::all();


        $TotalRecords = Client::count();


        $clients = Client::orderBy($columns[$request['order'][0]['column']], $request['order'][0]['dir'])
            ->offset($request['start'])
            ->limit($request['length'])
            ->get();

        $TotalFiltered = $TotalRecords;

        if ($clients) {
            $rows = [];
            foreach ($clients as $client) {

                $actions = '<div class="d-flex align-item-center">
                <a href="' . url('/edit-client', [$client->id]) . '" class="btn btn-sm p-2">
                   <i class="fas fa-pen-nib text-success"></i>
                </a>

                    <a href="#" class="btn btn-sm p-2">
                    <form action="' .  url('/delete-client', [$client->id]) . '" method="POST">
                    ' . method_field("DELETE") . '
                    ' . csrf_field() . '
                        <i class="fas fa-times text-danger remove"></i>
                    </form>
                    </a> </div>';


                $td = [];
                $td[] = $client->id;
                $td[] = $client->name;
                $td[] = $client->username;
                $td[] = $client->email;
                $td[] = $client->address;
                $td[] = $client->phone;
                $td[] = $actions;
                $rows[] = $td;
            }
            $json_data = array(
                "draw" => intval($request->draw),
                "recordsTotal" => intval($TotalRecords),
                "recordsFiltered" => intval($TotalFiltered),
                "data" => $rows,
            );
            echo json_encode($json_data);
        }
    }
}
