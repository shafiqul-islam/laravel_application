<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model
{
    use HasFactory;

    protected $table = 'activity_log';

    
    public function subjectID()
    {
        return $this->belongsTo('App\Models\Client' ,'subject_id');
    }
    public function causerID()
    {
        return $this->belongsTo('App\Models\User', 'causer_id');
    }
}
