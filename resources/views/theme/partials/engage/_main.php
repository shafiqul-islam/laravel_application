		<!--begin::Engage drawers-->

		<!--layout-partial:partials/engage/explore/_main.html-->
		<?php include(resource_path('/views/theme/partials/engage/explore/_main.php')); ?>



		<!--layout-partial:partials/engage/help/_main.html-->
		<?php include(resource_path('/views/theme/partials/engage/help/_main.php')); ?>


		<!--end::Engage drawers-->
		<!--begin::Engage toolbar-->
		<div class="engage-toolbar d-flex position-fixed px-5 fw-bolder zindex-2 top-50 end-0 transform-90 mt-20 gap-2">

			<!--layout-partial:partials/engage/explore/__toggle.html-->
			<?php include(resource_path('/views/theme/partials/engage/explore/__toggle.php')); ?>



			<!--layout-partial:partials/engage/help/__toggle.html-->
			<?php include(resource_path('/views/theme/partials/engage/help/__toggle.php')); ?>



			<!--layout-partial:partials/engage/purchase/_main.html-->
			<?php include(resource_path('/views/theme/partials/engage/purchase/_main.php')); ?>


		</div>
		<!--end::Engage toolbar-->