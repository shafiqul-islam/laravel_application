<?php include(resource_path('/views/theme/dashboard/header.php')) ?>

<div class="card shadow-sm">

    <div class="card-header border-bottom">
        <h3 class="card-title">Activity Logs</h3>
        
    </div>
    <div class="card-body">
        <table class="table align-middle table-row-dashed fs-6 gy-5 table-responsive dt_activity_log">
            <thead>
                <tr class="fw-bold fs-6 text-gray-800">
                    <th>ID</th>
                    <th>Log Name</th>
                    <th>Description</th>
                    <th>Subject Type</th>
                    <th>Event</th>
                    <th>Subject ID</th>
                    <th>Causer ID</th>
                    <th>Created At</th>
                    <th>Updated At</th>
                    <!-- <th class="">Action</th> -->
                </tr>
            </thead>
        </table>
    </div>
</div>


<?php include(resource_path('/views/theme/dashboard/footer.php')) ?>

<?php include(resource_path('/views/theme/Clients/template/add_client_modal.php')) ?>

<script>
    $(document).ready(function() {
        var url = "<?php echo url('server-side-logs'); ?>";

        var table = $('.dt_activity_log').DataTable({
            serverSide: true,
            processing: true,
            orderable: true,
            // searching:false,
            ajax: {
                url: url,
                type: "POST",
                data: {
                    "_token": content = "<?php echo csrf_token(); ?>",
                    // "_token": $('meta[name="csrf-token"]').attr('content'),
                },
            },
            error: function() {
                console.log(error);
            }
        });


        table.on('click', '.remove', function(e) {
            e.preventDefault();
            var deleteData = $(this).parent('form');

            Swal.fire({
                text: "Are You Sure! Want To Delete?",
                icon: "success",
                buttonsStyling: false,
                confirmButtonText: "Yes",
                customClass: {
                    confirmButton: "btn btn-danger"
                }
            }).then(function(actionType) {
                if (actionType.value == true) {
                    deleteData.submit();
                }
            });
        });



    });
</script>