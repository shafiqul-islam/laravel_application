<?php include(resource_path('/views/theme/dashboard/header.php')) ?>

<div class="card shadow-sm">
    <div class="card-header border-bottom">
        <h3 class="card-title">Permission</h3>
    </div>
    <div class="card-body permission">
        <div class="row">
            <div class="col-lg-3">
                <ul class="nav nav-tabs nav-pills border-0 flex-row flex-md-column me-5 mb-3 mb-md-0 fs-6">
                    <li class="nav-item w-md-150px me-0 text-center">
                        <a class="nav-link active rounded-pill" data-bs-toggle="tab" href="#kt_vtab_pane_1">Clients</a>
                    </li>
                    <li class="nav-item w-md-150px me-0 text-center">
                        <a class="nav-link rounded-pill" data-bs-toggle="tab" href="#kt_vtab_pane_2">Role</a>
                    </li>
                    <li class="nav-item w-md-150px text-center">
                        <a class="nav-link rounded-pill" data-bs-toggle="tab" href="#kt_vtab_pane_3">User</a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-9">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="kt_vtab_pane_1" role="tabpanel">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4>Clients</h4>
                                <span class="separator"></span>

                                <div class="form-check form-check-custom form-check-solid mt-3">
                                    <input class="form-check-input h-25px w-25px" type="checkbox" name="clients_module" id="flexCheckbox30" <?php echo (array_key_exists("clients_module", $permissions)) ? 'checked' : ''; ?> />
                                    <label class="form-check-label" for="flexCheckbox30">
                                        Module
                                    </label>
                                </div>
                                <div class="form-check form-check-custom form-check-solid mt-3">
                                    <input class="form-check-input h-25px w-25px" type="checkbox" name="clients_view" id="flexCheckbox30" />
                                    <label class="form-check-label" for="flexCheckbox30">
                                        View
                                    </label>
                                </div>
                                <div class="form-check form-check-custom form-check-solid mt-3">
                                    <input class="form-check-input h-25px w-25px" type="checkbox" name="clients_delete" id="flexCheckbox30" />
                                    <label class="form-check-label" for="flexCheckbox30">
                                        Delete
                                    </label>
                                </div>
                                <div class="form-check form-check-custom form-check-solid mt-3">
                                    <input class="form-check-input h-25px w-25px" type="checkbox" name="clients_edit" id="flexCheckbox30" />
                                    <label class="form-check-label" for="flexCheckbox30">
                                        Edit
                                    </label>
                                </div>
                                <div class="form-check form-check-custom form-check-solid mt-3">
                                    <input class="form-check-input h-25px w-25px" type="checkbox" name="clients_update" id="flexCheckbox30" />
                                    <label class="form-check-label" for="flexCheckbox30">
                                        Update
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="kt_vtab_pane_2" role="tabpanel">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4>Role</h4>
                                <span class="separator"></span>

                                <div class="form-check form-check-custom form-check-solid mt-3">
                                    <input class="form-check-input h-25px w-25px" type="checkbox" name="role_module" id="flexCheckbox30" />
                                    <label class="form-check-label" for="flexCheckbox30">
                                        Module
                                    </label>
                                </div>
                                <div class="form-check form-check-custom form-check-solid mt-3">
                                    <input class="form-check-input h-25px w-25px" type="checkbox" name="role_view" id="flexCheckbox30" />
                                    <label class="form-check-label" for="flexCheckbox30">
                                        View
                                    </label>
                                </div>
                                <div class="form-check form-check-custom form-check-solid mt-3">
                                    <input class="form-check-input h-25px w-25px" type="checkbox" name="role_delete" id="flexCheckbox30" />
                                    <label class="form-check-label" for="flexCheckbox30">
                                        Delete
                                    </label>
                                </div>
                                <div class="form-check form-check-custom form-check-solid mt-3">
                                    <input class="form-check-input h-25px w-25px" type="checkbox" name="role_edit" id="flexCheckbox30" />
                                    <label class="form-check-label" for="flexCheckbox30">
                                        Edit
                                    </label>
                                </div>
                                <div class="form-check form-check-custom form-check-solid mt-3">
                                    <input class="form-check-input h-25px w-25px" type="checkbox" name="role_update" id="flexCheckbox30" />
                                    <label class="form-check-label" for="flexCheckbox30">
                                        Update
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="kt_vtab_pane_3" role="tabpanel">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4>Users</h4>
                                <span class="separator"></span>

                                <div class="form-check form-check-custom form-check-solid mt-3">
                                    <input class="form-check-input h-25px w-25px" type="checkbox" name="users_module" id="flexCheckbox30" />
                                    <label class="form-check-label" for="flexCheckbox30">
                                        Module
                                    </label>
                                </div>
                                <div class="form-check form-check-custom form-check-solid mt-3">
                                    <input class="form-check-input h-25px w-25px" type="checkbox" name="users_view" id="flexCheckbox30" />
                                    <label class="form-check-label" for="flexCheckbox30">
                                        View
                                    </label>
                                </div>
                                <div class="form-check form-check-custom form-check-solid mt-3">
                                    <input class="form-check-input h-25px w-25px" type="checkbox" name="users_delete" id="flexCheckbox30" />
                                    <label class="form-check-label" for="flexCheckbox30">
                                        Delete
                                    </label>
                                </div>
                                <div class="form-check form-check-custom form-check-solid mt-3">
                                    <input class="form-check-input h-25px w-25px" type="checkbox" name="users_edit" id="flexCheckbox30" />
                                    <label class="form-check-label" for="flexCheckbox30">
                                        Edit
                                    </label>
                                </div>
                                <div class="form-check form-check-custom form-check-solid mt-3">
                                    <input class="form-check-input h-25px w-25px" type="checkbox" name="users_update" id="flexCheckbox30" />
                                    <label class="form-check-label" for="flexCheckbox30">
                                        Update
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
</div>


<?php include(resource_path('/views/theme/dashboard/footer.php')) ?>

<script>
    $(document).ready(function() {

        var nameID = $('.permission input[name]');

        nameID.on('click', function() {
            var role = $(this).attr('name');

            // console.log(role);

            var url = "<?php echo url('update-permission'); ?>";

            $.ajax({
                url: url,
                type: "POST",
                dataType: "Json",
                data: {
                    "_token": content = "<?php echo csrf_token(); ?>",
                    "role": role
                    // "_token": $('meta[name="csrf-token"]').attr('content'),
                },
                error: function() {
                    console.log(error);
                },
                success: function(data) {
                    console.log(data);
                },
            });
        });


    });
</script>