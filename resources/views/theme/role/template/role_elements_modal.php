<div class="modal-body p-10">
    <input type="hidden" name="id">
    
    <div class="mb-3">
        <label class="required form-label">Role Name</label>
        <input type="text" name="name" class="form-control form-control-solid p-2 mb-0" placeholder="Enter Role Name">
    </div>

    <div class=" modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>