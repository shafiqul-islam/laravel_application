<form action="<?php echo url('/add-role'); ?>" method="post">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
    <!-- Modal -->
    <div class="modal fade add_new_role_modal p-7" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header p-3">
                    <h3 class="modal-title" id="exampleModalLabel">Add Role</h3>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <?php include(resource_path('/views/theme/role/template/role_elements_modal.php')) ?>


            </div>
        </div>
    </div>
</form>