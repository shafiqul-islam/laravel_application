<?php include(resource_path('/views/theme/dashboard/header.php')) ?>

<div class="card shadow-sm">

    <div class="card-header border-bottom">
        <h3 class="card-title">Clients</h3>
        <div class="card-toolbar">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary btn-sm me-3" data-bs-toggle="modal" data-bs-target=".add_new_client_modal">
                Add New Client
            </button>
        </div>
    </div>
    <div class="card-body">
        <table class="table align-middle table-row-dashed fs-6 gy-5 table-responsive dt_clients">
            <thead>
                <tr class="fw-bold fs-6 text-gray-800">
                    <th>ID</th>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th class="">Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>


<?php include(resource_path('/views/theme/dashboard/footer.php')) ?>

<?php include(resource_path('/views/theme/Clients/template/add_client_modal.php')) ?>

<script>
    $(document).ready(function() {
        var url = "<?php echo url('server-side-clients'); ?>";

        var table = $('.dt_clients').DataTable({
            serverSide: true,
            processing: true,
            orderable: true,
            // searching:false,
            ajax: {
                url: url,
                type: "POST",
                data: {
                    "_token": content = "<?php echo csrf_token(); ?>",
                    // "_token": $('meta[name="csrf-token"]').attr('content'),
                },
            },
            error: function() {
                console.log(error);
            }
        });


        table.on('click', '.remove', function(e) {
            e.preventDefault();
            var deleteData = $(this).parent('form');

            Swal.fire({
                text: "Are You Sure! Want To Delete?",
                icon: "success",
                buttonsStyling: false,
                confirmButtonText: "Yes",
                customClass: {
                    confirmButton: "btn btn-danger"
                }
            }).then(function(actionType) {
                if (actionType.value == true) {
                    deleteData.submit();
                }
            });
        });



    });
</script>