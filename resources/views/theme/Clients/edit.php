<?php include(resource_path('/views/theme/dashboard/header.php')) ?>
<form action="<?php echo url('/update-client'); ?>" method="POST">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
    <div class="card shadow-sm">
        <div class="card-header">
            <h3 class="card-title">Update Client Profile</h3>

            <div class="card-toolbar">
                <button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target=".change_password_modal">
                    Change Password
                </button>
            </div>
        </div>
        <div class="card-body">
            <input type="hidden" name="id" value="<?php echo $editProfile->id; ?>">

            <div class="row d-flex justify-content-center mb-4">
                <div class="col-lg-6">
                    <label class="required form-label">Enter Name</label>
                    <input type="text" name="name" class="form-control form-control-solid p-2 text-gray-700 fw-bold" placeholder="Enter Name" value="<?php echo $editProfile->name; ?>">
                </div>
            </div>

            <div class="row d-flex justify-content-center mb-4">
                <div class="col-lg-6">
                    <label class="required form-label">Enter Username</label>
                    <input type="text" name="username" class="form-control form-control-solid p-2 text-gray-700 fw-bold" placeholder="Enter Username" value="<?php echo $editProfile->username; ?> ">
                </div>
            </div>

            <div class="row d-flex justify-content-center mb-4">
                <div class="col-lg-6">
                    <label class="required form-label">Enter Phone</label>
                    <input type="text" name="phone" class="form-control form-control-solid p-2 text-gray-700 fw-bold" placeholder="Enter Phone" value="<?php echo $editProfile->phone; ?>" />
                </div>
            </div>

            <div class="row d-flex justify-content-center mb-4">
                <div class="col-lg-6">
                    <label class="required form-label">Enter Email</label>
                    <input type="email" name="email" class="form-control form-control-solid p-2 text-gray-700 fw-bold" placeholder="Enter Email" value="<?php echo $editProfile->email; ?>">
                </div>
            </div>

            <div class="row d-flex justify-content-center mb-4">
                <div class="col-lg-6">
                    <label class="required form-label">Enter Address</label>
                    <input type="text" name="address" class="form-control form-control-solid p-2 text-gray-700 fw-bold" placeholder="Enter Address" value="<?php echo $editProfile->address; ?> ">
                </div>
            </div>

        </div>
        <div class="card-footer text-end">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>

</form>


<?php include(resource_path('/views/theme/dashboard/footer.php')) ?>

<?php // include(resource_path('/views/theme/Clients/template/change_password_modal.php')) ?>

<form action="<?php echo url('/change-password'); ?>" method="post">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
    <!-- Modal -->
    <div class="modal fade change_password_modal p-7" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

            <input type="hidden" name="id" value="<?php echo $editProfile->id; ?>">

                <div class="modal-header p-3">
                    <h3 class="modal-title" id="exampleModalLabel">Client</h3>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body p-10">
                    <div class="mb-3">
                        <label class="required form-label">Password</label>
                        <input type="password" name="password" class="form-control form-control-solid p-2 mb-0" placeholder="Enter Password">
                    </div>
                    <div class="mb-3">
                        <label class="required form-label">Confrim Username</label>
                        <input type="password" name="confirm_password" class="form-control form-control-solid p-2 mb-0" placeholder="Enter Confirm Password">
                    </div>

                    <div class=" modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>




