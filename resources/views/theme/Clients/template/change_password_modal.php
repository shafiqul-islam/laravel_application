<form action="<?php echo url('/change-password'); ?>" method="post">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
    <!-- Modal -->
    <div class="modal fade change_password_modal p-7" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

            <input type="hidden" name="id" value="<?php echo $profileData->id; ?>">

                <div class="modal-header p-3">
                    <h3 class="modal-title" id="exampleModalLabel">Client</h3>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body p-10">
                    <div class="mb-3">
                        <label class="required form-label">Password</label>
                        <input type="password" name="password" class="form-control form-control-solid p-2 mb-0" placeholder="Enter Password">
                    </div>
                    <div class="mb-3">
                        <label class="required form-label">Confrim Username</label>
                        <input type="password" name="confirm_password" class="form-control form-control-solid p-2 mb-0" placeholder="Enter Confirm Password">
                    </div>

                    <div class=" modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>