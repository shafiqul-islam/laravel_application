<form action="<?php echo url('/add-client'); ?>" method="post">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
    <!-- Modal -->
    <div class="modal fade add_new_client_modal p-7" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header p-3">
                    <h3 class="modal-title" id="exampleModalLabel">Add Client</h3>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body p-10">
                    <div class="mb-3">
                        <label class="required form-label">Enter Name</label>
                        <input type="text" name="name" class="form-control form-control-solid p-2 mb-0" placeholder="Enter Name">
                    </div>

                    <div class="mb-3">
                        <label class="required form-label">Enter Username</label>
                        <input type="text" name="username" class="form-control form-control-solid p-2 mb-0" placeholder="Enter Username">
                    </div>

                    <div class="mb-3">
                        <label class="required form-label">Enter Phone</label>
                        <input type="text" name="phone" class="form-control form-control-solid p-2 mb-0" placeholder="Enter Phone">
                    </div>

                    <div class="mb-3">
                        <label class="required form-label">Enter Email</label>
                        <input type="email" name="email" class="form-control form-control-solid p-2 mb-0" placeholder="Enter Email">
                    </div>

                    <div class="mb-3">
                        <label class="required form-label">Enter Password</label>
                        <input type="password" name="password" class="form-control form-control-solid p-2 mb-0" placeholder="Enter Password">
                    </div>

                    <div class="mb-3">
                        <label class="required form-label">Enter Address</label>
                        <input type="text" name="address" class="form-control form-control-solid p-2 mb-0" placeholder="Enter Address">
                    </div>
                    <div class=" modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>