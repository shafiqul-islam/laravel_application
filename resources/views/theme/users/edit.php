<?php include(resource_path('/views/theme/dashboard/header.php')) ?>

<?php if ($errors->any()) { ?>
    <div class="alert alert-danger">
        <ul>
           <?php foreach ($errors->all() as $error){ ?>
            <li>{{ $error }}</li>
           <?php } ?>
        </ul>
    </div>
<?php } ?>


<form action="<?php echo url('/update-user'); ?>" method="POST">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
    <div class="card shadow-sm">
        <div class="card-header">
            <h3 class="card-title">Update User</h3>

            <div class="card-toolbar">
                <button type="button" class="btn btn-primary btn-md me-3" data-bs-toggle="modal" data-bs-target=".change_photo_modal">
                    Change Photo
                </button>
                <button type="button" class="btn btn-info btn-md" data-bs-toggle="modal" data-bs-target=".change_password_modal">
                    Change Password
                </button>
            </div>
                
        </div>
        <div class="card-body">
            <input type="hidden" name="id" value="<?php echo $editProfile->id; ?>">

            <div class="row">
                <div class="col-lg-6">
                    <div class="mb-3">
                        <label class="required form-label">Enter Name</label>
                        <input type="text" name="name" class="form-control form-control-solid p-2 mb-0" value="<?php echo $editProfile->name; ?>" placeholder="Enter Name">
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="required form-label">Role</label>
                    <select class="form-select form-select-solid" name="role" data-placeholder="Select an option">

                        <option selected value="<?php $editProfile->role; ?>"><?php echo $roleName; ?> (*)</option>

                        <option value="">Select A Role</option>
                        <?php if (isset($roles) && !empty($roles) && count($roles)) { ?>
                            <?php foreach ($roles as $role) { ?>
                                <option value="<?php echo $role->id; ?>"><?php echo $role->name; ?></option>
                            <?php  } ?>

                        <?php  } ?>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="mb-3">
                        <label class="required form-label">Email <i class="fa fa-exclamation-circle" aria-hidden="true"></i></label>
                        <input type="email" name="email" class="form-control form-control-solid p-2 mb-0" value="<?php echo $editProfile->email; ?>" placeholder="Enter Email">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="mb-3">
                        <label class="required form-label">Phone <i class="fa fa-exclamation-circle" aria-hidden="true"></i></label>
                        <input type="text" name="phone" class="form-control form-control-solid p-2 mb-0" value="<?php echo $editProfile->phone; ?>" placeholder="Enter Phone">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="mb-3">
                        <label class="required form-label">Country <i class="fa fa-exclamation-circle" aria-hidden="true"></i></label>
                        <input type="text" name="country" class="form-control form-control-solid p-2 mb-0" value="<?php echo $editProfile->country; ?>" placeholder="Enter Country">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="mb-3">
                        <label class="required form-label">City <i class="fa fa-exclamation-circle" aria-hidden="true"></i></label>
                        <input type="text" name="city" class="form-control form-control-solid p-2 mb-0" value="<?php echo $editProfile->city; ?>" placeholder="Enter Phone">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="mb-3">
                        <label class="required form-label">Address <i class="fa fa-exclamation-circle" aria-hidden="true"></i></label>
                        <input type="text" name="address" class="form-control form-control-solid p-2 mb-0" value="<?php echo $editProfile->address; ?>" placeholder="Enter Address">
                    </div>
                </div>
            </div>

        </div>
        <div class="card-footer text-end">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>

</form>


<?php include(resource_path('/views/theme/dashboard/footer.php')) ?>

<!-- password change modal -->
<form action="<?php echo url('/change-user-password'); ?>" method="post">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
    <!-- Modal -->
    <div class="modal fade change_password_modal p-7" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <input type="hidden" name="id" value="<?php echo $editProfile->id; ?>">

                <div class="modal-header p-3">
                    <h3 class="modal-title" id="exampleModalLabel">Change Password</h3>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body p-10">
                    <div class="mb-3">
                        <label class="required form-label">Password</label>
                        <input type="password" name="password" class="form-control form-control-solid p-2 mb-0" placeholder="Enter Password">
                    </div>
                    <div class="mb-3">
                        <label class="required form-label">Confrim Username</label>
                        <input type="password" name="confirm_password" class="form-control form-control-solid p-2 mb-0" placeholder="Enter Confirm Password">
                    </div>

                    <div class=" modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>



<!-- change photo modal -->
<form action="<?php echo url('/change-user-photo'); ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
    <!-- Modal -->
    <div class="modal fade change_photo_modal p-7" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <input type="hidden" name="id" value="<?php echo $editProfile->id; ?>">

                <div class="modal-header p-3">
                    <h3 class="modal-title" id="exampleModalLabel">Change Photo</h3>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body p-10">
                    <div class="mb-3">
                        <label class="required form-label">Choose Photo</label>
                        <input type="file" name="photo" class="form-control form-control-solid p-2 mb-0" >
                    </div>

                    <div class=" modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>