<form action="<?php echo url('/add-user'); ?>" method="post">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>" />
    <!-- Modal -->
    <div class="modal fade add_user_modal p-7" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header p-3">
                    <h3 class="modal-title" id="exampleModalLabel">Add Client</h3>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body p-10">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="mb-3">
                                <label class="required form-label">Enter Name</label>
                                <input type="text" name="name" class="form-control form-control-solid p-2 mb-0" placeholder="Enter Name">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <label class="required form-label">Role</label>
                            <select class="form-select form-select-solid" name="role" data-placeholder="Select an option">
                                <option value="">Select A Role</option>
                                <?php if (isset($roles) && !empty($roles) && count($roles)) { ?>
                                    <?php foreach ($roles as $role) { ?>
                                        <option value="<?php echo $role->id; ?>"><?php echo $role->name; ?></option>
                                    <?php  } ?>

                                <?php  } ?>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="mb-3">
                                <label class="required form-label">Email <i class="fa fa-exclamation-circle" aria-hidden="true"></i></label>
                                <input type="email" name="email" class="form-control form-control-solid p-2 mb-0" placeholder="Enter Email">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-3">
                                <label class="required form-label">Phone <i class="fa fa-exclamation-circle" aria-hidden="true"></i></label>
                                <input type="text" name="phone" class="form-control form-control-solid p-2 mb-0" placeholder="Enter Phone">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="mb-3">
                                <label class="required form-label">Country <i class="fa fa-exclamation-circle" aria-hidden="true"></i></label>
                                <input type="text" name="country" class="form-control form-control-solid p-2 mb-0" placeholder="Enter Country">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-3">
                                <label class="required form-label">City <i class="fa fa-exclamation-circle" aria-hidden="true"></i></label>
                                <input type="text" name="city" class="form-control form-control-solid p-2 mb-0" placeholder="Enter Phone">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="mb-3">
                                <label class="required form-label">Address <i class="fa fa-exclamation-circle" aria-hidden="true"></i></label>
                                <input type="text" name="address" class="form-control form-control-solid p-2 mb-0" placeholder="Enter Address">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <label class="required form-label">Paasowrd <i class="fa fa-exclamation-circle" aria-hidden="true"></i></label>
                            <input type="password" name="password" class="form-control form-control-solid p-2 mb-0" placeholder="Enter Password">
                        </div>
                    </div>
                    <div class=" modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>