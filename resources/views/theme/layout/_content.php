						
						<!--begin::Container-->
						<div class="container-xxl" id="kt_content_container">
							
							<!--begin::Row-->
							<div class="row g-5 g-xl-10 mb-xl-10">
								<!--begin::Col-->
								<div class="col-md-6 col-lg-6 col-xl-6 col-xxl-3 mb-md-5 mb-xl-10">

<!--layout-partial:partials/widgets/cards/_widget-16.html-->


<!--layout-partial:partials/widgets/cards/_widget-7.html-->

								</div>
								<!--end::Col-->
								<!--begin::Col-->
								<div class="col-md-6 col-lg-6 col-xl-6 col-xxl-3 mb-md-5 mb-xl-10">

<!--layout-partial:partials/widgets/cards/_widget-17.html-->


<!--layout-partial:partials/widgets/lists/_widget-25.html-->

								</div>
								<!--end::Col-->
								<!--begin::Col-->
								<div class="col-lg-12 col-xl-12 col-xxl-6 mb-5 mb-xl-0">

<!--layout-partial:partials/widgets/timeline/_widget-3.html-->

								</div>
								<!--end::Col-->
							</div>
							<!--end::Row-->
							<!--begin::Row-->
							<div class="row g-5 g-xl-10 mb-5 mb-xl-10">
								<!--begin::Col-->
								<div class="col-xxl-6">

<!--layout-partial:partials/widgets/cards/_widget-18.html-->

								</div>
								<!--end::Col-->
								<!--begin::Col-->
								<div class="col-xxl-6">

<!--layout-partial:partials/widgets/engage/_widget-8.html-->

								</div>
								<!--end::Col-->
							</div>
							<!--end::Row-->
							<!--begin::Row-->
							<div class="row g-5 g-xl-10 mb-5 mb-xl-10">
								<!--begin::Col-->
								<div class="col-xl-4">

<!--layout-partial:partials/widgets/charts/_widget-35.html-->

								</div>
								<!--end::Col-->
								<!--begin::Col-->
								<div class="col-xl-8">

<!--layout-partial:partials/widgets/tables/_widget-14.html-->

								</div>
								<!--end::Col-->
							</div>
							<!--end::Row-->
							<!--begin::Row-->
							<div class="row g-5 g-xl-10">
								<!--begin::Col-->
								<div class="col-xl-4">

<!--layout-partial:partials/widgets/engage/_widget-1.html-->

								</div>
								<!--end::Col-->
								<!--begin::Col-->
								<div class="col-xl-8">

<!--layout-partial:partials/widgets/timeline/_widget-4.html-->

								</div>
								<!--end::Col-->
							</div>
							<!--end::Row-->
							
						</div>
						<!--end::Container-->
						