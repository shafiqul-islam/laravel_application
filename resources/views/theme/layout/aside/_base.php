				<!--begin::Aside-->
				<div id="kt_aside" class="aside aside-extended" data-kt-drawer="true" data-kt-drawer-name="aside" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="auto" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_aside_toggle">
					<!--begin::Primary-->
					<div class="aside-primary d-flex flex-column align-items-lg-center flex-row-auto">
						<!--begin::Logo-->
						<div class="aside-logo d-none d-lg-flex flex-column align-items-center flex-column-auto py-10" id="kt_aside_logo">
							<a href="?page=index">
								<img alt="Logo" src="assets/media/logos/logo-default.svg" class="h-50px" />
							</a>
						</div>
						<!--end::Logo-->
						<!--begin::Nav-->
						<div class="aside-nav d-flex flex-column align-items-center flex-column-fluid w-100 pt-5 pt-lg-0" id="kt_aside_nav">

							<!--layout-partial:layout/aside/__tabs.html-->
							<?php include(resource_path('/views/theme/layout/aside/__tabs.php')); ?>


						</div>
						<!--end::Nav-->
						<!--begin::Footer-->
						<div class="aside-footer d-flex flex-column align-items-center flex-column-auto" id="kt_aside_footer">
							<!--begin::Chat-->
							<div class="d-flex align-items-center mb-2">
								<!--begin::Menu wrapper-->
								<div class="btn btn-icon btn-custom" id="kt_drawer_chat_toggle">
									<!--begin::Svg Icon | path: icons/duotune/communication/com012.svg-->
									<span class="svg-icon svg-icon-2 svg-icon-lg-1">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
											<path opacity="0.3" d="M20 3H4C2.89543 3 2 3.89543 2 5V16C2 17.1046 2.89543 18 4 18H4.5C5.05228 18 5.5 18.4477 5.5 19V21.5052C5.5 22.1441 6.21212 22.5253 6.74376 22.1708L11.4885 19.0077C12.4741 18.3506 13.6321 18 14.8167 18H20C21.1046 18 22 17.1046 22 16V5C22 3.89543 21.1046 3 20 3Z" fill="currentColor" />
											<rect x="6" y="12" width="7" height="2" rx="1" fill="currentColor" />
											<rect x="6" y="7" width="12" height="2" rx="1" fill="currentColor" />
										</svg>
									</span>
									<!--end::Svg Icon-->
								</div>
								<!--end::Menu wrapper-->
							</div>
							<!--end::Chat-->
							<!--begin::Notifications-->
							<div class="d-flex align-items-center mb-2">
								<!--begin::Menu wrapper-->
								<div class="btn btn-icon btn-custom" data-kt-menu-trigger="click" data-kt-menu-overflow="true" data-kt-menu-placement="top-start" data-bs-toggle="tooltip" data-bs-placement="right" data-bs-dismiss="click" title="Notifications">
									<!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
									<span class="svg-icon svg-icon-2 svg-icon-lg-1">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
											<rect x="2" y="2" width="9" height="9" rx="2" fill="currentColor" />
											<rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2" fill="currentColor" />
											<rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2" fill="currentColor" />
											<rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2" fill="currentColor" />
										</svg>
									</span>
									<!--end::Svg Icon-->
								</div>

								<!--layout-partial:layout/topbar/partials/_notifications-menu.html-->
								<?php include(resource_path('/views/theme/layout/topbar/partials/_notifications-menu.php')); ?>


								<!--end::Menu wrapper-->
							</div>
							<!--end::Notifications-->
							<!--begin::Activities-->
							<div class="d-flex align-items-center mb-3">
								<!--begin::Drawer toggle-->
								<div class="btn btn-icon btn-custom" data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-overflow="true" data-kt-menu-placement="top-start" data-bs-toggle="tooltip" data-bs-placement="right" data-bs-dismiss="click" title="Activity Logs" id="kt_activities_toggle">
									<!--begin::Svg Icon | path: icons/duotune/general/gen032.svg-->
									<span class="svg-icon svg-icon-2 svg-icon-lg-1">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
											<rect x="8" y="9" width="3" height="10" rx="1.5" fill="currentColor" />
											<rect opacity="0.5" x="13" y="5" width="3" height="14" rx="1.5" fill="currentColor" />
											<rect x="18" y="11" width="3" height="8" rx="1.5" fill="currentColor" />
											<rect x="3" y="13" width="3" height="6" rx="1.5" fill="currentColor" />
										</svg>
									</span>
									<!--end::Svg Icon-->
								</div>
								<!--end::drawer toggle-->
							</div>
							<!--end::Activities-->
							<!--begin::Theme mode-->
							<div class="d-flex align-items-center mb-3">

								<!--layout-partial:partials/theme-mode/_main.html-->
								<?php include(resource_path('/views/theme/partials/theme-mode/_main.php')); ?>


							</div>
							<!--end::Theme mode-->
							<!--begin::User-->
							<div class="d-flex align-items-center mb-10" id="kt_header_user_menu_toggle">
								<!--begin::Menu wrapper-->
								<div class="cursor-pointer symbol symbol-40px" data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-overflow="true" data-kt-menu-placement="top-start">
									<img src="assets/media/avatars/300-1.jpg" alt="image" />
								</div>

								<!--layout-partial:partials/menus/_user-account-menu.html-->
								<?php include(resource_path('/views/theme/partials/menus/_user-account-menu.php')); ?>


								<!--end::Menu wrapper-->
							</div>
							<!--end::User-->
						</div>
						<!--end::Footer-->
					</div>
					<!--end::Primary-->
					<!--begin::Secondary-->
					<div class="aside-secondary d-flex flex-row-fluid">
						<!--begin::Workspace-->
						<div class="aside-workspace my-5 p-5" id="kt_aside_wordspace">

							<!--layout-partial:layout/aside/__tab-contents/_base.html-->
							<?php include(resource_path('/views/theme/layout/aside/__tab-contents/_base.php')); ?>


						</div>
						<!--end::Workspace-->
					</div>
					<!--end::Secondary-->
					<!--begin::Aside Toggle-->
					<button class="btn btn-sm btn-icon bg-body btn-color-gray-600 btn-active-primary position-absolute translate-middle start-100 end-0 bottom-0 shadow-sm d-none d-lg-flex" data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body" data-kt-toggle-name="aside-minimize" style="margin-bottom: 1.35rem">
						<!--begin::Svg Icon | path: icons/duotune/arrows/arr063.svg-->
						<span class="svg-icon svg-icon-2 rotate-180">
							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
								<rect opacity="0.5" x="6" y="11" width="13" height="2" rx="1" fill="currentColor" />
								<path d="M8.56569 11.4343L12.75 7.25C13.1642 6.83579 13.1642 6.16421 12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75L5.70711 11.2929C5.31658 11.6834 5.31658 12.3166 5.70711 12.7071L11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25C13.1642 17.8358 13.1642 17.1642 12.75 16.75L8.56569 12.5657C8.25327 12.2533 8.25327 11.7467 8.56569 11.4343Z" fill="currentColor" />
							</svg>
						</span>
						<!--end::Svg Icon-->
					</button>
					<!--end::Aside Toggle-->
				</div>
				<!--end::Aside-->