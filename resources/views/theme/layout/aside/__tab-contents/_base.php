							<div class="d-flex h-100 flex-column">
								<!--begin::Wrapper-->
								<div class="flex-column-fluid hover-scroll-y" data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-height="auto" data-kt-scroll-wrappers="#kt_aside_wordspace" data-kt-scroll-dependencies="#kt_aside_secondary_footer" data-kt-scroll-offset="0px">
									<!--begin::Tab content-->
									<div class="tab-content">
										<!--begin::Tab pane-->
										<div class="tab-pane fade active show" id="kt_aside_nav_tab_projects" role="tabpanel">

											<!--layout-partial:layout/aside/__tab-contents/__projects.html-->
											<?php include(resource_path('/views/theme/layout/aside/__tab-contents/__projects.php')); ?>


										</div>
										<!--end::Tab pane-->
										<!--begin::Tab pane-->
										<div class="tab-pane fade" id="kt_aside_nav_tab_menu" role="tabpanel">

											<!--layout-partial:layout/aside/__tab-contents/__menu.html-->
											<?php include(resource_path('/views/theme/layout/aside/__tab-contents/__menu.php')); ?>


										</div>
										<!--end::Tab pane-->
										<!--begin::Tab pane-->
										<div class="tab-pane fade" id="kt_aside_nav_tab_subscription" role="tabpanel">

											<!--layout-partial:layout/aside/__tab-contents/__subscription.html-->
											<?php include(resource_path('/views/theme/layout/aside/__tab-contents/__subscription.php')); ?>


										</div>
										<!--end::Tab pane-->
										<!--begin::Tab pane-->
										<div class="tab-pane fade" id="kt_aside_nav_tab_tasks" role="tabpanel">

											<!--layout-partial:layout/aside/__tab-contents/__tasks.html-->
											<?php include(resource_path('/views/theme/layout/aside/__tab-contents/__tasks.php')); ?>


										</div>
										<!--end::Tab pane-->
										<!--begin::Tab pane-->
										<div class="tab-pane fade" id="kt_aside_nav_tab_notifications" role="tabpanel">

											<!--layout-partial:layout/aside/__tab-contents/__notifications.html-->
											<?php include(resource_path('/views/theme/layout/aside/__tab-contents/__notifications.php')); ?>


										</div>
										<!--end::Tab pane-->
										<!--begin::Tab pane-->
										<div class="tab-pane fade" id="kt_aside_nav_tab_authors" role="tabpanel">

											<!--layout-partial:layout/aside/__tab-contents/__authors.html-->
											<?php include(resource_path('/views/theme/layout/aside/__tab-contents/__authors.php')); ?>


										</div>
										<!--end::Tab pane-->
									</div>
									<!--end::Tab content-->
								</div>
								<!--end::Wrapper-->
								<!--begin::Footer-->
								<div class="flex-column-auto pt-10 px-5" id="kt_aside_secondary_footer">
									<a href="?page=documentation/getting-started" class="btn btn-bg-light btn-color-gray-600 btn-flex btn-active-color-primary flex-center w-100" data-bs-toggle="tooltip" data-bs-custom-class="tooltip-dark" data-bs-trigger="hover" data-bs-offset="0,5" data-bs-dismiss-="click" title="200+ in-house components and 3rd-party plugins">
										<span class="btn-label">Docs &amp; Components</span>
										<!--begin::Svg Icon | path: icons/duotune/general/gen005.svg-->
										<span class="svg-icon btn-icon svg-icon-4 ms-2">
											<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
												<path opacity="0.3" d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM12.5 18C12.5 17.4 12.6 17.5 12 17.5H8.5C7.9 17.5 8 17.4 8 18C8 18.6 7.9 18.5 8.5 18.5L12 18C12.6 18 12.5 18.6 12.5 18ZM16.5 13C16.5 12.4 16.6 12.5 16 12.5H8.5C7.9 12.5 8 12.4 8 13C8 13.6 7.9 13.5 8.5 13.5H15.5C16.1 13.5 16.5 13.6 16.5 13ZM12.5 8C12.5 7.4 12.6 7.5 12 7.5H8C7.4 7.5 7.5 7.4 7.5 8C7.5 8.6 7.4 8.5 8 8.5H12C12.6 8.5 12.5 8.6 12.5 8Z" fill="currentColor" />
												<rect x="7" y="17" width="6" height="2" rx="1" fill="currentColor" />
												<rect x="7" y="12" width="10" height="2" rx="1" fill="currentColor" />
												<rect x="7" y="7" width="6" height="2" rx="1" fill="currentColor" />
												<path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="currentColor" />
											</svg>
										</span>
										<!--end::Svg Icon-->
									</a>
								</div>
								<!--end::Footer-->
							</div>