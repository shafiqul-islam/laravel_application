											
											<!--begin::Wrapper-->
											<div class="m-0">
												<!--begin::Toolbar-->
												<div class="d-flex mb-10">

<!--layout-partial:partials/search/_inline.html-->
<?php include(resource_path('/views/theme/partials/search/_inline.php')); ?>


													<!--begin::Filter-->
													<div class="flex-shrink-0 ms-2">
														<!--begin::Menu toggle-->
														<button type="button" class="btn btn-icon btn-bg-light btn-active-icon-primary btn-color-gray-400" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
															<!--begin::Svg Icon | path: icons/duotune/general/gen031.svg-->
															<span class="svg-icon svg-icon-2">
																<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																	<path d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z" fill="currentColor" />
																</svg>
															</span>
															<!--end::Svg Icon-->
														</button>
														<!--end::Menu toggle-->
														<!--begin::Menu-->

<!--layout-partial:partials/menus/_menu-1.html-->
<?php include(resource_path('/views/theme/partials/menus/_menu-1.php')); ?>


														<!--end::Menu-->
													</div>
													<!--end::Filter-->
												</div>
												<!--end::Toolbar-->
												<!--begin::Projects-->
												<div class="m-0">
													<!--begin::Heading-->
													<h1 class="text-gray-800 fw-bold mb-6 mx-5">Projects</h1>
													<!--end::Heading-->
													<!--begin::Items-->
													<div class="mb-10">
														<!--begin::Item-->
														<a href="?page=apps/projects/project" class="custom-list d-flex align-items-center px-5 py-4">
															<!--begin::Symbol-->
															<div class="symbol symbol-40px me-5">
																<span class="symbol-label">
																	<img src="assets/media/svg/brand-logos/bebo.svg" class="h-50 align-self-center" alt="" />
																</span>
															</div>
															<!--end::Symbol-->
															<!--begin::Description-->
															<div class="d-flex flex-column flex-grow-1">
																<!--begin::Title-->
																<h5 class="custom-list-title fw-bold text-gray-800 mb-1">Briviba SaaS</h5>
																<!--end::Title-->
																<!--begin::Link-->
																<span class="text-gray-400 fw-bold">By James</span>
																<!--end::Link-->
															</div>
															<!--begin::Description-->
														</a>
														<!--end::Item-->
														<!--begin::Item-->
														<a href="?page=apps/projects/project" class="custom-list d-flex align-items-center px-5 py-4">
															<!--begin::Symbol-->
															<div class="symbol symbol-40px me-5">
																<span class="symbol-label">
																	<img src="assets/media/svg/brand-logos/vimeo.svg" class="h-50 align-self-center" alt="" />
																</span>
															</div>
															<!--end::Symbol-->
															<!--begin::Description-->
															<div class="d-flex flex-column flex-grow-1">
																<!--begin::Title-->
																<h5 class="custom-list-title fw-bold text-gray-800 mb-1">Vine Quick Reports</h5>
																<!--end::Title-->
																<!--begin::Link-->
																<span class="text-gray-400 fw-bold">By Andres</span>
																<!--end::Link-->
															</div>
															<!--begin::Description-->
														</a>
														<!--end::Item-->
														<!--begin::Item-->
														<a href="?page=apps/projects/project" class="custom-list d-flex align-items-center px-5 py-4">
															<!--begin::Symbol-->
															<div class="symbol symbol-40px me-5">
																<span class="symbol-label">
																	<img src="assets/media/svg/brand-logos/kickstarter.svg" class="h-50 align-self-center" alt="" />
																</span>
															</div>
															<!--end::Symbol-->
															<!--begin::Description-->
															<div class="d-flex flex-column flex-grow-1">
																<!--begin::Title-->
																<h5 class="custom-list-title fw-bold text-gray-800 mb-1">KC Account CRM</h5>
																<!--end::Title-->
																<!--begin::Link-->
																<span class="text-gray-400 fw-bold">By Keenthemes</span>
																<!--end::Link-->
															</div>
															<!--begin::Description-->
														</a>
														<!--end::Item-->
														<!--begin::Item-->
														<a href="?page=apps/projects/project" class="custom-list d-flex align-items-center px-5 py-4">
															<!--begin::Symbol-->
															<div class="symbol symbol-40px me-5">
																<span class="symbol-label">
																	<img src="assets/media/svg/brand-logos/beats-electronics.svg" class="h-50 align-self-center" alt="" />
																</span>
															</div>
															<!--end::Symbol-->
															<!--begin::Description-->
															<div class="d-flex flex-column flex-grow-1">
																<!--begin::Title-->
																<h5 class="custom-list-title fw-bold text-gray-800 mb-1">Baloon SaaS</h5>
																<!--end::Title-->
																<!--begin::Link-->
																<span class="text-gray-400 fw-bold">By SIA Team</span>
																<!--end::Link-->
															</div>
															<!--begin::Description-->
														</a>
														<!--end::Item-->
														<!--begin::Item-->
														<a href="?page=apps/projects/project" class="custom-list d-flex align-items-center px-5 py-4">
															<!--begin::Symbol-->
															<div class="symbol symbol-40px me-5">
																<span class="symbol-label">
																	<img src="assets/media/svg/brand-logos/rgb.svg" class="h-50 align-self-center" alt="" />
																</span>
															</div>
															<!--end::Symbol-->
															<!--begin::Description-->
															<div class="d-flex flex-column flex-grow-1">
																<!--begin::Title-->
																<h5 class="custom-list-title fw-bold text-gray-800 mb-1">RGB Cloud System</h5>
																<!--end::Title-->
																<!--begin::Link-->
																<span class="text-gray-400 fw-bold">By Andrei</span>
																<!--end::Link-->
															</div>
															<!--begin::Description-->
														</a>
														<!--end::Item-->
														<!--begin::Item-->
														<a href="?page=apps/projects/project" class="custom-list d-flex align-items-center px-5 py-4">
															<!--begin::Symbol-->
															<div class="symbol symbol-40px me-5">
																<span class="symbol-label">
																	<img src="assets/media/svg/brand-logos/disqus.svg" class="h-50 align-self-center" alt="" />
																</span>
															</div>
															<!--end::Symbol-->
															<!--begin::Description-->
															<div class="d-flex flex-column flex-grow-1">
																<!--begin::Title-->
																<h5 class="custom-list-title fw-bold text-gray-800 mb-1">Disqus Forum</h5>
																<!--end::Title-->
																<!--begin::Link-->
																<span class="text-gray-400 fw-bold">By Disqus Inc.</span>
																<!--end::Link-->
															</div>
															<!--begin::Description-->
														</a>
														<!--end::Item-->
														<!--begin::Item-->
														<a href="?page=apps/projects/project" class="custom-list d-flex align-items-center px-5 py-4">
															<!--begin::Symbol-->
															<div class="symbol symbol-40px me-5">
																<span class="symbol-label">
																	<img src="assets/media/svg/brand-logos/plurk.svg" class="h-50 align-self-center" alt="" />
																</span>
															</div>
															<!--end::Symbol-->
															<!--begin::Description-->
															<div class="d-flex flex-column flex-grow-1">
																<!--begin::Title-->
																<h5 class="custom-list-title fw-bold text-gray-800 mb-1">Proove Quick CRM</h5>
																<!--end::Title-->
																<!--begin::Link-->
																<span class="text-gray-400 fw-bold">By Proove Limited</span>
																<!--end::Link-->
															</div>
															<!--begin::Description-->
														</a>
														<!--end::Item-->
													</div>
													<!--end::Items-->
												</div>
												<!--end::Projects-->
											</div>
											<!--end::Wrapper-->
											