											
											<!--begin::Authors-->
											<div class="mx-5">
												<!--begin::Header-->
												<h3 class="fw-bolder text-dark mx-0 mb-10">Authors</h3>
												<!--end::Header-->
												<!--begin::Body-->
												<div class="mb-12">
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<!--begin::Avatar-->
														<div class="symbol symbol-50px me-5">
															<img src="assets/media/avatars/300-6.jpg" class="" alt="" />
														</div>
														<!--end::Avatar-->
														<!--begin::Text-->
														<div class="flex-grow-1">
															<a href="?page=pages/projects/users" class="text-dark fw-bolder text-hover-primary fs-6">Emma Smith</a>
															<span class="text-muted d-block fw-bold">Project Manager</span>
														</div>
														<!--end::Text-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<!--begin::Avatar-->
														<div class="symbol symbol-50px me-5">
															<img src="assets/media/avatars/300-5.jpg" class="" alt="" />
														</div>
														<!--end::Avatar-->
														<!--begin::Text-->
														<div class="flex-grow-1">
															<a href="?page=pages/projects/users" class="text-dark fw-bolder text-hover-primary fs-6">Sean Bean</a>
															<span class="text-muted d-block fw-bold">PHP, SQLite, Artisan CLI</span>
														</div>
														<!--end::Text-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<!--begin::Avatar-->
														<div class="symbol symbol-50px me-5">
															<img src="assets/media/avatars/300-11.jpg" class="" alt="" />
														</div>
														<!--end::Avatar-->
														<!--begin::Text-->
														<div class="flex-grow-1">
															<a href="?page=pages/projects/users" class="text-dark fw-bolder text-hover-primary fs-6">Brian Cox</a>
															<span class="text-muted d-block fw-bold">HTML5, jQuery, CSS3</span>
														</div>
														<!--end::Text-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<!--begin::Avatar-->
														<div class="symbol symbol-50px me-5">
															<img src="assets/media/avatars/300-23.jpg" class="" alt="" />
														</div>
														<!--end::Avatar-->
														<!--begin::Text-->
														<div class="flex-grow-1">
															<a href="?page=pages/projects/users" class="text-dark fw-bolder text-hover-primary fs-6">Dan Wilson</a>
															<span class="text-muted d-block fw-bold">MangoDB, Java</span>
														</div>
														<!--end::Text-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<!--begin::Avatar-->
														<div class="symbol symbol-50px me-5">
															<img src="assets/media/avatars/300-10.jpg" class="" alt="" />
														</div>
														<!--end::Avatar-->
														<!--begin::Text-->
														<div class="flex-grow-1">
															<a href="?page=pages/projects/users" class="text-dark fw-bolder text-hover-primary fs-6">Natali Trump</a>
															<span class="text-muted d-block fw-bold">NET, Oracle, MySQL</span>
														</div>
														<!--end::Text-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center mb-7">
														<!--begin::Avatar-->
														<div class="symbol symbol-50px me-5">
															<img src="assets/media/avatars/300-9.jpg" class="" alt="" />
														</div>
														<!--end::Avatar-->
														<!--begin::Text-->
														<div class="flex-grow-1">
															<a href="?page=pages/projects/users" class="text-dark fw-bolder text-hover-primary fs-6">Francis Mitcham</a>
															<span class="text-muted d-block fw-bold">React, Vue</span>
														</div>
														<!--end::Text-->
													</div>
													<!--end::Item-->
													<!--begin::Item-->
													<div class="d-flex align-items-center">
														<!--begin::Avatar-->
														<div class="symbol symbol-50px me-5">
															<img src="assets/media/avatars/300-12.jpg" class="" alt="" />
														</div>
														<!--end::Avatar-->
														<!--begin::Text-->
														<div class="flex-grow-1">
															<a href="?page=pages/projects/users" class="text-dark fw-bolder text-hover-primary fs-6">Jessie Clarcson</a>
															<span class="text-muted d-block fw-bold">Angular, React</span>
														</div>
														<!--end::Text-->
													</div>
													<!--end::Item-->
												</div>
												<!--end::Body-->
											</div>
											<!--end::Authors-->
											