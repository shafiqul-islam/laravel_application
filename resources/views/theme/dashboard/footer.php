<?php include(resource_path('/views/theme/dashboard/master_footer.php')); ?>


<script>
	var hostUrl = "<?php echo asset('theme/assets/') ?>";
</script>

<!--begin::Javascript-->
<!--begin::Global Javascript Bundle(used by all pages)-->
<script src="<?php echo asset('/theme/assets/plugins/global/plugins.bundle.js') ?>"></script>
<script src="<?php echo asset('/theme/assets/js/scripts.bundle.js') ?>"></script>
<!--end::Global Javascript Bundle-->
<!--begin::Page Vendors Javascript(used by this page)-->
<script src="<?php asset('/theme/assets/plugins/custom/fullcalendar/fullcalendar.bundle.js') ?>"></script>
<!--end::Page Vendors Javascript-->
<!--begin::Page Custom Javascript(used by this page)-->
<script src="<?php echo asset('/theme/assets/js/custom/widgets.js'); ?>"></script>
<!--end::Page Custom Javascript-->
<!--end::Javascript-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<!-- <script src="<?php //asset('/theme/assets/plugins/custom/datatables/datatables.bundle.js') ?>"></script> -->

</body>
<!--end::Body-->

</html>