		<!--begin::Main-->
		<!--begin::Root-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="page d-flex flex-row flex-column-fluid">

				<?php include(resource_path('/views/theme/layout/aside/_base.php')); ?>


				<!--begin::Wrapper-->
				<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">

					<!--layout-partial:layout/header/_base.html-->
				<?php include(resource_path('/views/theme/layout/header/_base.php')); ?>


					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">


						<!--begin::Container-->
						<div class="container-xxl" id="kt_content_container">