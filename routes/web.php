<?php

use App\Http\Controllers\ActivityLogController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LikeController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});

Route::get('/home', [HomeController::class, 'index']);

Route::get('/like', [LikeController::class, 'index']);
Route::post('/store', [LikeController::class, 'store']);



// clients routs
Route::get('/clients',[ClientController::class,'index']);
Route::post('/server-side-clients',[ClientController::class,'fetchByAjaxAllClients']);
Route::post('/add-client',[ClientController::class,'add']);
Route::get('/edit-client/{id}',[ClientController::class,'edit']);
Route::post('/update-client',[ClientController::class,'update']);
Route::delete('/delete-client/{id}',[ClientController::class,'destroy']);
Route::post('/change-password',[ClientController::class,'changePassword']);


// activity log routs
Route::get('/activity-log',[ActivityLogController::class,'index']);
Route::post('/server-side-logs',[ActivityLogController::class,'fetchByAjaxLogs']);


// role routs
Route::get('/all-role',[RoleController::class,'index']);
Route::post('/server-side-roles',[RoleController::class,'fetchByAjaxAllRoles']);
Route::post('/add-role',[RoleController::class,'add']);
Route::get('/edit-client/{id}',[RoleController::class,'edit']);
Route::post('/update-role',[RoleController::class,'update']);
Route::delete('/delete-role/{id}',[RoleController::class,'destroy']);
Route::post('/fetch-role-data',[RoleController::class,'fetchRoleData']);
Route::get('/all-permissions/{id}',[RoleController::class,'permission']);
Route::post('/update-permission',[RoleController::class,'updatePermission']);


// users routs
Route::get('/all-users',[UserController::class,'index']);
Route::post('/server-side-users',[UserController::class,'fetchByAjaxAllUsers']);
Route::post('/add-user',[UserController::class,'add']);
Route::get('/edit-user/{id}',[UserController::class,'edit']);
Route::post('/update-user',[UserController::class,'update']);
Route::delete('/delete-user/{id}',[UserController::class,'destroy']);
Route::post('/change-user-password',[UserController::class,'changeUserPassword']);
Route::post('/change-user-photo',[UserController::class,'changePhoto']);












